#!/usr/bin/env python3
"""Simple program to track the time I spend on school"""


import argparse
from collections import defaultdict, deque
from datetime import datetime
import itertools
from pathlib import Path


def last(iterable):
    try:
        return deque(iterable, maxlen=1).pop()
    except IndexError:
        return tuple()


def positive_int(string):
    if string.isnumeric() and int(string) > 0:
        return int(string)
    else:
        msg = "invalid positive int value: '{}'".format(string)
        raise argparse.ArgumentTypeError(msg)


def time_pairs(logfile):
    with logfile.open(encoding="UTF-8") as log:
        starts, stops = itertools.tee(log)
        starts = [
            datetime.fromisoformat(ts.strip())
            for ts in itertools.islice(starts, 0, None, 2)
        ]
        stops = [
            datetime.fromisoformat(ts.strip())
            for ts in itertools.islice(stops, 1, None, 2)
        ]
        return itertools.zip_longest(starts, stops)


def log_time(logfile):
    now = datetime.now().isoformat(timespec="minutes")
    print(
        "Logging [{}] {} time: {}".format(
            logfile.resolve().name,
            "start" if None not in last(time_pairs(logfile)) else "stop",
            now,
        )
    )
    with logfile.open(mode="a", encoding="UTF-8") as log:
        print(now, file=log)


def elapsed(logfile):
    now = datetime.now().replace(second=0, microsecond=0)
    last_entry = last(time_pairs(logfile))
    previous = now if None not in last_entry else last_entry[0]
    time = (now - previous).seconds
    total_minutes = time // 60
    hours, minutes = divmod(total_minutes, 60)
    return ["Current elapsed time is {}:{:>02}".format(hours, minutes)]


def time_intervals(logfile, format_string, time_fn_name):
    time_periods = defaultdict(int)
    for start, stop in time_pairs(logfile):
        time_period = format_string.format(*getattr(start, time_fn_name)())
        try:
            time = stop - start
        except TypeError:
            pass
        else:
            time_periods[time_period] += time.seconds
    text = []
    for time_period, total_seconds in time_periods.items():
        total_minutes = total_seconds // 60
        hours, minutes = divmod(total_minutes, 60)
        text.append("{}: {:>2}:{:>02}".format(time_period, hours, minutes))
    return text


def days(logfile):
    return time_intervals(logfile, "{}-{:>02}-{:>02}", "timetuple")


def weeks(logfile):
    return time_intervals(logfile, "{}-{:>02}", "isocalendar")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Log & view time spent on a project",
        epilog="Logs the current time to the default logfile when run with"
        " no arguments",
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "-e",
        "--elapsed",
        action="store_true",
        default=None,
        help="prints the current elapsed time to the console",
    )
    group.add_argument(
        "-d",
        "--days",
        nargs="?",
        const=0,
        default=None,
        type=positive_int,
        help="prints a log of daily time spent in the past 'num_days' "
        "days to the console (defaults to all logged days)",
        metavar="num_days",
    )
    group.add_argument(
        "-w",
        "--weeks",
        nargs="?",
        const=0,
        default=None,
        type=positive_int,
        help="prints a log of weekly time spent in the past 'num_weeks'"
        " weeks to the console (defaults to all logged weeks)",
        metavar="num_weeks",
    )
    group.add_argument(
        "-D",
        "--default",
        action="store_true",
        help="sets 'logfile' as the new default",
    )
    parser.add_argument(
        "logfile",
        nargs=argparse.REMAINDER,
        default=".default",
        help="name of log file;"
        " defaults to file pointed to by .default symlink",
    )
    args = parser.parse_args()
    logfile = ' '.join(args.logfile)
    default = Path("~/.timelog/.default").expanduser()
    if args.default:
        if logfile == ".default":
            parser.error("must include logfile when setting default")
        else:
            default.unlink()
            default.symlink_to(default.with_name(logfile))
            parser.exit(message=f"Set new default file: {logfile}\n")
    elif not default.is_symlink():
        default.unlink()
        default.symlink_to(default.with_name("default"))
    logfile = default.with_name(logfile) if logfile else default
    logfile.touch()
    lines = None
    if args.elapsed is not None:
        lines = elapsed(logfile)
    elif args.days is not None:
        lines = days(logfile)[-args.days :]
    elif args.weeks is not None:
        lines = weeks(logfile)[-args.weeks :]

    if lines is not None:
        print(f"[{logfile.resolve().name}]")
        print("\n".join(lines))
    else:
        log_time(logfile)

    if logfile.lstat().st_size == 0:
        logfile.unlink()
